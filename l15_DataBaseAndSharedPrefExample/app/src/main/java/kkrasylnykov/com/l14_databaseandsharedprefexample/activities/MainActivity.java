package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.AppSettings;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout m_conteynerLinearLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_conteynerLinearLayout = (LinearLayout) findViewById(R.id.coteynerLinearLayoutMainActivity);
        View btnAdd = findViewById(R.id.addButtonMainActivity);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.removeAllButtonMainActivity);
        btnRemoveAll.setOnClickListener(this);

        //Создаем класс настроек
        AppSettings appSettings = new AppSettings(this);
        //Проверяем - первый раз мы запускаем приложение или нет
        if (appSettings.getIsFirstStart()){
            appSettings.setIsFirstStart(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        //Отчищаем контейнер
        m_conteynerLinearLayout.removeAllViews();
        DBHelper dbHelper = new DBHelper(this);
        //Получаем базу данных для чтения
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //Формируем запрос к БД
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, null, null, null, null, null);
        //Если Cursor не null
        if (cursor!=null){
            try{
                //Проверяем, может ли Cursor перейти на первый элемент
                if (cursor.moveToFirst()){
                    //Создаем параметры отображения элемента
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    do{
                        //Считываем из курсора данные по каждому элементу
                        long id = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_ID));
                        String strName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_PHONE));
                        String strDate = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_DATE));
                        //Создаем текст вью
                        TextView textView = new TextView(this);
                        textView.setText("strName: " + strName +
                                "\nstrSName: " + strSName + "\nstrPhone: " + strPhone+"\n" + "\nstrDate: " + strDate+"\n");
                        //Устанавливаем параметры отображения
                        textView.setLayoutParams(params);
                        textView.setOnClickListener(this);
                        textView.setTag(id);
                        //Добавляем элемент в контейнер
                        m_conteynerLinearLayout.addView(textView);
                    } while (cursor.moveToNext()); //Выходим из цикла в случае если Cursor не может перейти к следующему элементу (достиг конца или потерял связь с данными)
                }
            } finally {
                //После окончания работы с Cursor - ОБЗАТЕЛЬНО ЕГО ЗАКРЫТЬ!
                cursor.close();
            }
        }
        db.close();
        dbHelper.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButtonMainActivity:
                Intent intent = new Intent(this, ComposeActivity.class);
                startActivity(intent);
                break;
            case R.id.removeAllButtonMainActivity:
                DBHelper dbHelper = new DBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                //Удаление всех данных из таблицы
                db.delete(DBConstants.TABLE_NAME,null,null);
                db.close();
                dbHelper.close();
                updateList();
                break;
            default:
                if (v.getClass().getSimpleName().equals("TextView")){
                    long nId = (long)v.getTag();
                    Intent intentEdit = new Intent(this, ComposeActivity.class);
                    intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, nId);
                    startActivity(intentEdit);
                }
        }
    }
}
