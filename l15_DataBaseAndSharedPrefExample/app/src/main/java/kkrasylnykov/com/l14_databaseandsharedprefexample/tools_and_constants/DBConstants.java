package kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants;

/**Класс для хранения констант для базы данных.
 * Содержит константы, для работы с БД.
 */
public class DBConstants {

    public static final String TABLE_NAME = "Contacts";

    public static final String TABLE_CONTACTS_FIELD_ID = "_id";
    public static final String TABLE_CONTACTS_FIELD_NAME = "_name";
    public static final String TABLE_CONTACTS_FIELD_SNAME = "_sname";
    public static final String TABLE_CONTACTS_FIELD_PHONE = "_phone";
    public static final String TABLE_CONTACTS_FIELD_DATE = "_date";
}
