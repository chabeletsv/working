package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;


public class ComposeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_ID = "EXTRA_KEY_ID";

    private long m_nId = -1;

    private EditText m_nameEditText;
    private EditText m_snameEditText;
    private EditText m_phoneEditText;
    EditText currentDateTime;
    Calendar dateAndTime=Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        Button btnAdd = (Button) findViewById(R.id.addButtonComposeActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.removeButtonComposeActivity);
        btnRemove.setOnClickListener(this);

        m_nameEditText = (EditText) findViewById(R.id.nameEditTextComposeActivity);
        m_snameEditText = (EditText) findViewById(R.id.snameEditTextComposeActivity);
        m_phoneEditText = (EditText) findViewById(R.id.phoneEditTextComposeActivity);
        currentDateTime=(EditText)findViewById(R.id.currentDateTime);
        setInitialDateTime();

        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nId = bundle.getLong(EXTRA_KEY_ID,-1);
            }
        }

        if (m_nId!=-1){
            btnAdd.setText("Update");

            btnRemove.setVisibility(View.VISIBLE);

            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            //Указываем, какие поля в таблице нас интересуют
            String[] arrFilds = {DBConstants.TABLE_CONTACTS_FIELD_NAME,
                    DBConstants.TABLE_CONTACTS_FIELD_SNAME,
                    DBConstants.TABLE_CONTACTS_FIELD_PHONE,
                    DBConstants.TABLE_CONTACTS_FIELD_DATE};

            //Указываем строку запроса
            String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
            //Указываем параметры строки запроса
            String[] arrSelectionArgs = {Long.toString(m_nId)};
            Cursor cursor = db.query(DBConstants.TABLE_NAME, arrFilds,
                    strSelection, arrSelectionArgs, null, null, null);
            if (cursor!=null){
                try{
                    if (cursor.moveToFirst()){
                        String strName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_PHONE));
                        String strDate = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_DATE));

                        m_nameEditText.setText(strName);
                        m_snameEditText.setText(strSName);
                        m_phoneEditText.setText(strPhone);
                        currentDateTime.setText(strDate);

                    }
                } finally {
                    cursor.close();
                }
            }
            db.close();
            dbHelper.close();
        }
    }

    @Override
    public void onClick(View v) {
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (v.getId()){
            case R.id.addButtonComposeActivity:
                ContentValues values = new ContentValues();
                values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, m_nameEditText.getText().toString());
                values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, m_snameEditText.getText().toString());
                values.put(DBConstants.TABLE_CONTACTS_FIELD_PHONE, m_phoneEditText.getText().toString());
                values.put(DBConstants.TABLE_CONTACTS_FIELD_DATE, currentDateTime.getText().toString());

                if(m_nId!=-1){
                    //Указываем строку запроса
                    String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
                    //Указываем параметры строки запроса
                    String[] arrSelectionArgs = {Long.toString(m_nId)};
                    //Выполняем обновление данных
                    db.update(DBConstants.TABLE_NAME,values,
                            strSelection, arrSelectionArgs);
                } else {
                    //Выполняем вставку данных
                    db.insert(DBConstants.TABLE_NAME,null,values);
                }
                break;

            case R.id.removeButtonComposeActivity:
                String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
                String[] arrSelectionArgs = {Long.toString(m_nId)};
                db.delete(DBConstants.TABLE_NAME,strSelection,arrSelectionArgs);
                break;
        }

        db.close();
        dbHelper.close();

        finish();
    }

    // установка начальных даты и времени
    private void setInitialDateTime() {

        currentDateTime.setText(DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_SHOW_TIME));
    }

    // отображаем диалоговое окно для выбора даты
    public void setDate(View v) {
        new DatePickerDialog(ComposeActivity.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };
}
